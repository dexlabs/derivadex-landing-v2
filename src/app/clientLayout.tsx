'use client';

import { ColorModeScript } from '@chakra-ui/react';
import { usePathname } from 'next/navigation';
import { useEffect } from 'react';
import ReactGA from 'react-ga4';
import { useNavigationEvent } from 'src/hooks/useNavigationEvent';

import { Providers } from './providers';

// Chakra Provider requires to use client context
export default function ClientLayout({ children }: { children: React.ReactNode }) {
    const pathname = usePathname();
    useNavigationEvent((path) => {
        ReactGA.send({ hitType: 'pageview', page: path });
    });

    useEffect(() => {
        const googleAnalyticsID = process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID;
        if (!googleAnalyticsID) {
            return;
        }

        ReactGA.initialize(googleAnalyticsID);
        ReactGA.send({ hitType: 'pageview', page: pathname });
    }, []);

    return (
        <>
            <ColorModeScript initialColorMode="light" />
            <Providers>{children}</Providers>
        </>
    );
}
