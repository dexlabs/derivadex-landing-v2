import './globals.css';

import type { Metadata } from 'next';

import ClientLayout from './clientLayout';

export const metadata: Metadata = {
    title: 'DerivaDEX',
    description: 'DerivaDEX is a decentralized exchange with the performance and security traders need.',
    icons: {
        icon: 'assets/icons/ddx_logo.png',
        shortcut: 'assets/icons/ddx_logo.png',
    },
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
    return (
        <html lang="en">
            <body>
                <ClientLayout>{children}</ClientLayout>
            </body>
        </html>
    );
}
