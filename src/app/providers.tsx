'use client';
import { CacheProvider } from '@chakra-ui/next-js';
import { ChakraProvider } from '@chakra-ui/react';
import { Global } from '@emotion/react';

import theme, { GlobalStyle } from './theme';

export function Providers({ children }: { children: React.ReactNode }) {
    return (
        <CacheProvider>
            <ChakraProvider theme={theme}>
                <Global styles={GlobalStyle} />
                {children}
            </ChakraProvider>
        </CacheProvider>
    );
}
