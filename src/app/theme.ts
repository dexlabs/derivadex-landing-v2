import { extendTheme } from '@chakra-ui/react';
import { css } from '@emotion/react';

export default extendTheme({
    config: {
        initialColorMode: 'light',
        useSystemColorMode: false,
    },

    colors: {
        pink: {
            50: '#f194b3',
            100: '#f194b3',
            200: '#f194b3',
            300: '#f194b3',
            400: '#ef88aa',
            500: '#ef88aa',
            600: '#ef88aa',
            700: '#d77a99',
            800: '#d77a99',
            900: '#d77a99',
        },
        lavender: {
            50: '#b1a1ef',
            100: '#b1a1ef',
            200: '#b1a1ef',
            300: '#b1a1ef',
            400: '#a896ed',
            500: '#a896ed',
            600: '#a896ed',
            700: '#8678be',
            800: '#8678be',
            900: '#8678be',
        },
        indigo: {
            50: '#422d8d',
            100: '#422d8d',
            200: '#422d8d',
            300: '#422d8d',
            400: '#2d1680',
            500: '#2d1680',
            600: '#2d1680',
            700: '#291473',
            800: '#291473',
            900: '#291473',
        },
        purple: {
            50: '#816bf0',
            100: '#816bf0',
            200: '#816bf0',
            300: '#816bf0',
            400: '#735AEE',
            500: '#735AEE',
            600: '#735AEE',
            700: '#6851d6',
            800: '#6851d6',
            900: '#6851d6',
        },
        green: {
            50: '#6bc0ae',
            100: '#6bc0ae',
            200: '#6bc0ae',
            300: '#6bc0ae',
            400: '#5AB9A5',
            500: '#5AB9A5',
            600: '#5AB9A5',
            700: '#51a795',
            800: '#51a795',
            900: '#51a795',
        },
        brand: {
            transparentGray: {
                50: 'rgba(221, 221, 221, 0.4)',
                100: 'rgba(221, 221, 221, 0.4)',
                200: 'rgba(221, 221, 221, 0.4)',
                300: 'rgba(199, 199, 199, 0.4)',
                400: 'rgba(177, 177, 177, 0.4)',
            },
        },
    },

    components: {
        Input: {
            variants: {
                outline: {
                    field: {
                        _focus: {
                            borderColor: 'lavender.500',
                            boxShadow: 'lavender.500 0 0 0 1px',
                        },
                    },
                },
            },
        },

        Select: {
            variants: {
                outline: {
                    field: {
                        _focus: {
                            borderColor: 'lavender.500',
                            boxShadow: 'lavender.500 0 0 0 1px',
                        },
                    },
                },
            },
        },
        Button: {
            baseStyle: {
                _focus: {
                    boxShadow: '0 0 0 3px #81869a',
                },
            },
        },
        CloseButton: {
            baseStyle: {
                _focus: {
                    boxShadow: '0 0 0 3px #b3b6c2', //#81869a',
                },
            },
        },
        Popover: {
            parts: [],
            baseStyle: {
                content: {
                    _focus: {
                        boxShadow: '0 0 0 3px #b3b6c2',
                    },
                },
            },
        },
    },
});

export const GlobalStyle = css`
    @font-face {
        font-family: 'SF-Pro';
        font-weight: normal;
        font-style: normal;
        src: local('SF Pro Display Regular'),
            url('https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-regular-webfont.woff2')
                format('woff2');
        font-display: swap;
    }

    @font-face {
        font-family: 'SF-Pro';
        font-weight: 500;
        src: local('SF Pro Display Medium'),
            url('https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-medium-webfont.woff2')
                format('woff2');
        font-display: swap;
    }

    @font-face {
        font-family: 'SF-Pro';
        font-weight: 600;
        src: local('SF Pro Display Semibold'),
            url('https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-semibold-webfont.woff2')
                format('woff2');
        font-display: swap;
    }

    @font-face {
        font-family: 'SF-Pro';
        font-weight: bold;
        src: local('SF Pro Display Bold'),
            url('https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-bold-webfont.woff2')
                format('woff2');
        font-display: swap;
    }

    @font-face {
        font-family: 'SF-Pro';
        font-weight: 800;
        src: local('SF Pro Display Heavy'),
            url('https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-heavy-webfont.woff2')
                format('woff2');
        font-display: swap;
    }

    @font-face {
        font-family: 'SF-Pro-Text';
        font-style: normal;
        font-weight: normal;
        src: local('SF Pro Text Regular'),
            url('https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscotext-regular-webfont.woff2')
                format('woff2');
        font-display: swap;
    }

    @font-face {
        font-family: 'SF-Pro-Text';
        font-style: normal;
        font-weight: 500;
        src: local('SF Pro Text Medium'),
            url('https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscotext-medium-webfont.woff2')
                format('woff2');
        font-display: swap;
    }

    @font-face {
        font-family: 'SF-Pro-Text';
        font-style: normal;
        font-weight: 600;
        src: local('SF Pro Text Semibold'),
            url('https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscotext-semibold-webfont.woff2')
                format('woff2');
        font-display: swap;
    }

    @font-face {
        font-family: 'SF Pro Text';
        font-style: normal;
        font-weight: bold;
        src: local('SF Pro Text Bold')
            url('https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscotext-bold-webfont.woff2')
            format('woff2');
        font-display: swap;
    }

    @font-face {
        font-family: 'SF-Pro-Text';
        font-style: normal;
        font-weight: 800;
        src: local('SF Pro Text Heavy')
            url('https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscotext-heavy-webfont.woff2')
            format('woff2');
        font-display: swap;
    }
    * {
        box-sizing: border-box;
    }

    html,
    body {
        *::-moz-selection {
            background: rgba(49, 60, 94, 0.1);
        }
        *::selection {
            background: rgba(49, 60, 94, 0.1);
        }
        margin: 0;
        padding: 0;
        min-width: 240px;
        scroll-behavior: smooth;
        text-rendering: optimizeSpeed;
        position: relative;
        background-repeat: no-repeat;
        background-color: white;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

    body {
        font-family: 'SF-Pro', Arial, Helvetica, sans-serif;
        overflow-x: hidden;
        text-rendering: optimizeSpeed;
        *::-moz-selection {
            background: rgba(193, 197, 207, 1);
        }
        *::selection {
            background: rgba(193, 197, 207, 1);
        }
    }
`;
