import { Box, Heading, Img, Text } from '@chakra-ui/react';

export default function Elavator() {
    return (
        <>
            <Box as="section" color="#211646" w="100%" m="0 auto 1rem">
                <Box p="2rem 1rem">
                    <Box position="relative" maxW="1000px" m="0 auto" w="100%" textAlign="left">
                        <Text fontWeight="600" fontSize={['1.125rem']}>
                            What is DerivaDEX?
                        </Text>
                        <Heading
                            fontWeight="800"
                            maxW="40rem"
                            my="1rem"
                            fontSize={['1.5rem', '2rem', '2.5rem']}
                            lineHeight={['1.5rem', '2rem', '2.5rem']}
                        >
                            Until now, traders have had to choose between performance and security.
                        </Heading>

                        <Text
                            maxW={['25rem', '', '40rem']}
                            mb="1rem"
                            fontFamily="SF-Pro-Text"
                            fontSize={['0.75rem', '1rem', '1.25rem']}
                            lineHeight={['1rem', '1.5rem', '1.625rem']}
                        >
                            Centralized exchanges offer strong liquidity and products, but at the expense of user
                            control.
                            <br />
                            Decentralized exchanges give control to users, but compromise on usability.
                        </Text>
                    </Box>
                </Box>
                <Img src={'/assets/heros/hero1.svg'} top={0} left={0} pb="2rem" alt="DerivaDEX Hero 1" />
            </Box>
            <Box as="section" color="#211646" mt="2rem">
                <Box p="2rem 1rem">
                    <Box m="0 auto" maxW="1000px" w="100%" position="relative" textAlign="left">
                        <Heading
                            fontWeight="800"
                            mb="1rem"
                            maxW="25rem"
                            fontSize={['1.5rem', '2rem', '2.5rem']}
                            lineHeight={['1.5rem', '2rem', '2.5rem']}
                        >
                            DerivaDEX is the best of both worlds.
                        </Heading>

                        <Text
                            maxW={['25rem', '', '30rem']}
                            fontFamily="SF-Pro-Text"
                            fontSize={['0.75rem', '1rem', '1.25rem']}
                            lineHeight={['1rem', '1.5rem', '1.625rem']}
                        >
                            DerivaDEX is a decentralized exchange with the performance and security traders need.
                        </Text>
                    </Box>
                </Box>
                <Img src={'/assets/heros/hero2.svg'} top={0} left={0} pb="2rem" alt="DerivaDEX Hero 2" />
            </Box>
        </>
    );
}
