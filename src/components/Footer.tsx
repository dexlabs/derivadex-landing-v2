'use client';

import {
    Box,
    Button,
    Flex,
    Icon,
    IconButton,
    Input,
    InputGroup,
    InputLeftElement,
    Stack,
    Text,
} from '@chakra-ui/react';
import { useState } from 'react';
import { FiCheck, FiMail, FiUser } from 'react-icons/fi';

import getIcon from './AssetHelper';

interface ISite {
    name: string;
    href: string;
}

export default function Footer(): JSX.Element {
    const text = '#211646';
    const gradient = 'linear-gradient(to right, #f7fbfb 0%, #f7fbfb 50%, #f7f6fa 50%, #f2eff8 100%)';
    const gradient2 = 'linear-gradient(#f7f6fa, #f2eff8)';
    const input = 'white';

    const sites: ISite[] = [
        {
            name: 'Twitter',
            href: 'https://twitter.com/DDX_Official',
        },
        {
            name: 'Discord',
            href: 'https://discord.com/invite/uR2Kt4Q',
        },
        {
            name: 'Medium',
            href: 'https://medium.com/derivadex',
        },
    ];

    function Heading({ text }: { text: string }) {
        return (
            <Text fontSize={['2rem', '2rem', '2.5rem']} fontWeight="800" m="1rem 0" textAlign="center">
                {text}
            </Text>
        );
    }

    function SocialIcon({ site }: { site: ISite }) {
        return (
            <IconButton
                icon={getIcon(site.name)}
                aria-label={site.name}
                appearance="none"
                alignItems="center"
                display="inline-flex"
                justifyContent="center"
                outline="none"
                whiteSpace="nowrap"
                userSelect="none"
                overflow="visible"
                position="relative"
                verticalAlign="middle"
                textAlign="center"
                textTransform="none"
                backgroundColor="white"
                border="1px solid #f7f8f9"
                borderRadius="1000rem"
                boxShadow="rgba(8, 11, 14, 0.06) 0 0 1px 0, rgba(8, 11, 14, 0.1) 0 3px 3px -1px"
                h="3.5rem"
                w="3.5rem"
                m="0 0.5rem"
                p="0 1rem"
                transitionDuration="1s"
                _hover={{
                    color: '#f8f8f8',
                    boxShadow: 'rgba(0, 0, 0, 0.22) 0px 19px 43px',
                    transform: 'translate3d(0px, -1px, 0px)',
                }}
                _focus={{
                    boxShadow: 'none',
                }}
                as="a"
                target={'_blank'}
                rel="noopener noreferrer"
                href={site.href}
            />
        );
    }

    const [inputVals, setInputVals] = useState({ name: '', email: '' });
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [hasSubmitted, setHasSubmitted] = useState(false);

    function onSubmit(event: any) {
        event.preventDefault();
        setIsSubmitting(true);
        setTimeout(() => setIsSubmitting(false), 1000);
        setHasSubmitted(true);
    }

    return (
        <Box as="footer" p={['0', '0', '2rem']} bg={[gradient2, gradient2, gradient]} color={text}>
            <Flex
                flexDirection={['column', 'column', 'row']}
                justify={['center', 'center', 'space-between']}
                alignItems="flex-start"
                w="100%"
                maxW="1000px"
                m="0 auto"
                p="0.5rem"
            >
                <Flex
                    flexDirection="column"
                    flex={1}
                    justify="center"
                    mb={['1rem', '', '0rem']}
                    w={['100%', '', '50%']}
                >
                    <>
                        <Heading text={'Get in touch!'} />
                        <Box textAlign="center" m="0 auto">
                            {sites.map((site: any, index: number) => {
                                return <SocialIcon site={site} key={index} />;
                            })}
                        </Box>
                    </>
                </Flex>
                <Flex
                    flexDirection="column"
                    flex={1}
                    justify="center"
                    mb={['1rem', '', '0rem']}
                    w={['100%', '', '50%']}
                    textAlign="center"
                >
                    <>
                        <Heading text={'Get early access!'} />

                        <script src="https://f.convertkit.com/ckjs/ck.5.js" />
                        <form
                            action="https://app.convertkit.com/forms/1400965/subscriptions"
                            method="post"
                            data-sv-form="1400965"
                            data-uid="b18d5ec2a7"
                            data-format="inline"
                            data-version="5"
                            onSubmit={onSubmit}
                        >
                            <Flex flexDirection="column" justifyContent="center" alignItems="center" textAlign="center">
                                <Stack spacing={3} mx="auto">
                                    <InputGroup>
                                        {/* eslint-disable-next-line */}
                                        <InputLeftElement
                                            pointerEvents="none"
                                            children={<Icon as={FiUser} boxSize={4} />}
                                        />
                                        <Input
                                            name="fields[first_name]"
                                            aria-label={'Your first name'}
                                            placeholder={'Your first name'}
                                            sx={{
                                                '::placeholder': {
                                                    color: 'gray.400',
                                                },
                                            }}
                                            type="text"
                                            backgroundColor={input}
                                            value={inputVals.name}
                                            onChange={(event: any) =>
                                                setInputVals({ ...inputVals, name: event.target.value })
                                            }
                                            isRequired
                                            autoComplete="off"
                                            min={1}
                                            maxLength={80}
                                        />
                                    </InputGroup>
                                    <InputGroup>
                                        {/* eslint-disable-next-line */}
                                        <InputLeftElement
                                            pointerEvents="none"
                                            children={<Icon as={FiMail} boxSize={4} />}
                                        />
                                        <Input
                                            name="email_address"
                                            aria-label={'Your email address'}
                                            placeholder={'Your email address'}
                                            sx={{
                                                '::placeholder': {
                                                    color: 'gray.400', // Specify your desired color here
                                                },
                                            }}
                                            type="email"
                                            backgroundColor={input}
                                            value={inputVals.email}
                                            onChange={(event: any) =>
                                                setInputVals({ ...inputVals, email: event.target.value })
                                            }
                                            isRequired
                                            autoComplete="off"
                                            min={1}
                                            maxLength={80}
                                            bgColor="white"
                                            border="1px solid #e2e8f0"
                                            color="#211646"
                                        />
                                    </InputGroup>
                                </Stack>
                                <Button
                                    m="1rem auto"
                                    color="white"
                                    colorScheme={'indigo'}
                                    aria-label={'Subscribe'}
                                    leftIcon={hasSubmitted ? <FiCheck /> : undefined}
                                    type="submit"
                                    isDisabled={!inputVals.name || !inputVals.email}
                                    isLoading={isSubmitting}
                                    loadingText={'Subscribe'}
                                >
                                    {hasSubmitted ? 'Thank You!' : 'Subscribe'}
                                </Button>
                            </Flex>
                        </form>
                    </>
                </Flex>
            </Flex>
        </Box>
    );
}
