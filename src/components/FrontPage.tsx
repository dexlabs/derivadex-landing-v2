'use client';
import { Flex, Text } from '@chakra-ui/react';
import Scroll from 'react-scroll';

import Elavator from './Elevator';
import Footer from './Footer';
import Launchpad from './Launchpad';
import Navbar from './navbar';
import Partners from './Partners';
import Summary from './Summary';

export default function FrontPage() {
    return (
        <Flex flexDirection={'column'} className="front-page">
            <Navbar />
            <Flex
                color="#211646"
                w="min(1000px, 100%)"
                m="0 auto"
                textAlign="center"
                flexDirection="column"
                flexWrap="wrap"
                px="1rem"
                pb="0.5rem"
            >
                <Text
                    as="h1"
                    fontSize="clamp(3rem, 6.25vw, 5.625rem)"
                    lineHeight="clamp(3rem, 5.98vw, 5.375rem)"
                    fontWeight="800"
                    m="6rem auto 0"
                    w={['100%', '90%', '85%']}
                >
                    The next generation of crypto derivatives
                </Text>
                <Text
                    as="h2"
                    fontSize="clamp(1rem, 1.4vw, 1.25rem)"
                    lineHeight="clamp(1.5rem, 3.61vw, 2.25rem)"
                    m={['1.5rem auto', '1.5rem auto', '2rem auto 0.5rem']}
                    maxW="45rem"
                    fontFamily="SF-Pro-Text"
                >
                    Discover the future of trading with DerivaDEX, a community-governed derivatives exchange that unites
                    performance and autonomy.
                </Text>
            </Flex>

            <Flex justifyContent={'center'} flexDirection="column">
                <Scroll.Element name="products">
                    <Launchpad />
                </Scroll.Element>
                <Scroll.Element name="whatIsDerivadex?">
                    <Elavator />
                </Scroll.Element>
                <Summary />
                <Partners />
                <Scroll.Element name="contact">
                    <Footer />
                </Scroll.Element>
            </Flex>
        </Flex>
    );
}
