import { Box, Button, Flex, Grid, Text } from '@chakra-ui/react';

export default function Section1(): JSX.Element {
    const applications = [
        {
            title: 'Cryptocurrency Exchange',
            link: 'https://testnet.derivadex.io/',
            bg: '/assets/gradients/gradient3.svg',
            enabled: true,
            id: 1,
        },
        {
            title: 'DerivaDEX Stats',
            link: 'https://testnet.derivadex.io/stats',
            bg: '/assets/gradients/statsgradient.png',
            enabled: true,
            id: 2,
        },
        {
            title: 'Governance App',
            link: 'https://governance.derivadex.com/',
            bg: '/assets/gradients/gradient2.svg',
            enabled: true,
            id: 3,
        },
    ];

    function Card({
        title,
        bg,
        link,
        enabled,
        id,
    }: {
        title: string;
        bg: number;
        link: string;
        enabled?: boolean;
        id: number;
    }) {
        return (
            <Flex
                position="relative"
                m={['0', '', '0rem']}
                minH="clamp(100px, 27.78vw, 400px)"
                minW="clamp(100px, 15.625vw, 250px)"
                flexDirection="column"
                justifyContent="flex-start"
                boxShadow="rgba(0,0,0,.1) 0 1px 3px 0,rgba(0,0,0,.06) 0 1px 2px 0"
                borderRadius="0.5rem"
                backgroundImage={`url(${bg})`}
                backgroundSize="cover"
                backgroundPosition={['0', '0%', '50% 70%']}
            >
                <Flex
                    flexDirection={['row', 'row', 'column']}
                    position={['relative', 'relative', 'absolute']}
                    alignItems="stretch"
                    justifyContent={['space-between', 'space-between', 'stretch']}
                    top={['', '', '1.5rem']}
                    left={['', '', '1.5rem']}
                    m={['0rem', '', '0 auto']}
                    p={['1rem', '', '0rem']}
                >
                    <Text
                        fontWeight="800"
                        color="white"
                        fontSize="clamp(1.25rem, 2.5vw, 2.25rem)"
                        lineHeight="clamp(1.75rem, 3.195vw, 2.875rem)"
                        overflowWrap="normal"
                        maxW={['58%', '', '80%']}
                    >
                        {title}
                    </Text>

                    <Flex align="center" justifyContent={['', '', 'flex-start']} mt={['', '', '0.5rem']}>
                        <Button
                            as="a"
                            colorScheme={id === 1 ? 'pink' : id === 2 ? 'purple' : 'green'}
                            color="white"
                            isDisabled={!enabled}
                            href={link}
                            target={enabled ? '_blank' : ''}
                            aria-label={title}
                            rel="noopener noreferrer"
                        >
                            Launch
                        </Button>

                        {!enabled && (
                            <Text
                                display={['none', 'none', 'inline']}
                                color="white"
                                ml="1rem"
                                fontFamily="SF-Pro-Text"
                                fontSize="0.75rem"
                            >
                                Coming soon
                            </Text>
                        )}
                    </Flex>
                </Flex>
            </Flex>
        );
    }

    return (
        <Box as="section" maxW="1000px" w="100%" m="0 auto" py={['1rem', '', '2rem']} px={['1rem', '', '0rem']}>
            <Grid
                gridGap="1rem"
                gridTemplateAreas={['"card" "card" "card"', '"card" "card" "card"', '"card card card"']}
            >
                {applications.map((app: any, index: number) => {
                    return (
                        <Card
                            key={index}
                            title={app.title}
                            bg={app.bg}
                            link={app.link}
                            enabled={app.enabled}
                            id={app.id}
                        />
                    );
                })}
            </Grid>
        </Box>
    );
}
