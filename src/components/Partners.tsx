import { Box, Flex, Img, Link, Text } from '@chakra-ui/react';

interface IPartner {
    label: string;
    link: string;
    src: string;
}

export default function Partners() {
    const partnersArray: IPartner[] = [
        {
            label: 'CMS Holdings LLC',
            link: 'http://cmsholdings.io/',
            src: '/assets/partners/cms.svg',
        },
        {
            label: 'DragonFly Capital',
            link: 'https://www.dcp.capital/',
            src: '/assets/partners/dc.svg',
        },
        {
            label: 'Electric Capital',
            link: 'https://www.electriccapital.com/',
            src: '/assets/partners/electric.svg',
        },
        {
            label: 'Coinbase Ventures',
            link: 'https://ventures.coinbase.com/',
            src: '/assets/partners/coinbase.svg',
        },
        {
            label: 'Polychain Capital',
            link: 'https://polychain.capital/',
            src: '/assets/partners/polychain.svg',
        },
    ];

    function Sqaure({ partner }: { partner: IPartner }) {
        return (
            <Link
                p="0.5rem"
                color="#626262"
                flexBasis={['100%', '50%', '33.3333%', '16.6666666667%']}
                maxW={['100%', '50%', '33.3333%', '16.6666666667%']}
                _focus={{ boxShadow: 'none' }}
                _hover={{ color: '#626262' }}
                href={partner.link}
                isExternal
                aria-label={partner.label}
            >
                <Flex
                    alignItems="center"
                    flexDirection="column"
                    justifyContent="center"
                    m="0"
                    h={['5rem', '5rem', '5rem', '10rem']}
                    px={['0.75rem', '', '', '0.75rem']}
                    py={['0', '', '', '0.75rem']}
                >
                    <Img src={partner.src} alt={partner.label} h="5rem" w="100%" />
                </Flex>
            </Link>
        );
    }

    return (
        <Box as="section" color="#211646" p="2rem 1rem">
            <Box m="0 auto" maxW="1000px" w="100%" mb="0">
                <Text fontSize="1.125rem" fontWeight="600" m="0 auto" textAlign="left">
                    Supported by
                </Text>
                <Flex
                    m="0 auto"
                    maxW="82rem"
                    flexDirection="row"
                    flexWrap="wrap"
                    py={['1rem', '0']}
                    justifyContent="center"
                >
                    {partnersArray.map((partner: IPartner, index: number) => {
                        return <Sqaure partner={partner} key={index} />;
                    })}
                </Flex>
            </Box>
        </Box>
    );
}
