import { Box, Flex, Grid, Heading, Icon, Text } from '@chakra-ui/react';

import { Chart, Jar, Scale, Token } from './AssetHelper';

interface ICard {
    title: string;
    description: string;
    icon: any;
    gridArea: number;
}

export default function Summary() {
    const cards = [
        {
            title: 'Exchange',
            description:
                'DerivaDEX is a new kind of exchange with key performance advantages, including a <b>real-time price feed, fast trade resolution, and a competitive fee structure.</b>',
            icon: Chart,
            gridArea: 1,
        },
        {
            title: 'Governance App',
            description:
                'DerivaDEX is a <b>DAO from Day 1.</b> Traders and token holders control the platform. Users vote on features relating to the exchange.',
            icon: Scale,
            gridArea: 2,
        },
        {
            title: 'DDX Token',
            description:
                'DDX is the native token of DerivaDEX.\nDDX is used to govern the project via the DerivaDAO. DDX is also used for fee reductions and for staking opportunities.',
            icon: Token,
            gridArea: 3,
        },
        {
            title: 'Insurance Mining',
            description:
                'Insurance mining <b>bootstraps capital</b> for the insurance fund. Users <b>stake</b> to the insurance fund and receive DDX. The insurance fund provides a <b>world-class trader experience.</b>',
            icon: Jar,
            gridArea: 4,
        },
    ];

    function CardComponent({ title, description, icon, gridArea }: ICard) {
        return (
            <Flex
                flexDirection="column"
                p="2rem"
                borderRadius="0.25rem"
                bg="#F7F7FB"
                boxShadow="rgba(0,0,0,.1) 0 1px 3px 0,rgba(0,0,0,.06) 0 1px 2px 0"
                gridArea={gridArea}
            >
                <Icon as={icon} boxSize={[8, 8, 16]} />
                <Heading fontSize={['1rem', '1.5rem', '2.25rem']} fontWeight="800" my={['0.75rem', '1rem', '1.5rem']}>
                    {title}
                </Heading>

                <Text
                    lineHeight={['1rem', '1.5rem', '1.625rem']}
                    fontSize={['0.6rem', '0.75rem', '1rem']}
                    fontFamily="SF-Pro-Text"
                    whiteSpace="pre-line"
                >
                    <span
                        dangerouslySetInnerHTML={{
                            __html: description,
                        }}
                    />
                </Text>
            </Flex>
        );
    }

    return (
        <Box as="section" color="#211646" p="2rem 1rem">
            <Box m="0 auto" maxW="1000px" w="100%" mb="0">
                <Text fontSize="1.125rem" fontWeight="600" mb="1.5rem">
                    What we're building
                </Text>

                <Grid
                    gap={4}
                    gridTemplateColumns={['1fr', '', 'repeat(2, 1fr)']}
                    gridTemplateRows={['repeat(24 1fr)', '', 'repeat(2, 1fr)']}
                    gridTemplateAreas={["'1','2','3','4'", '', "'1 2','3 4'"]}
                >
                    {cards.map((card: ICard, i: number) => {
                        return (
                            <CardComponent
                                key={i}
                                title={card.title}
                                description={card.description}
                                icon={card.icon}
                                gridArea={card.gridArea}
                            />
                        );
                    })}
                </Grid>
            </Box>
        </Box>
    );
}
