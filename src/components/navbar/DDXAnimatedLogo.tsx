import styled from '@emotion/styled';
import { motion } from 'framer-motion';

const pathVariants1 = {
    hidden: {
        opacity: 0,
        pathLength: 0,
    },
    visible: {
        opacity: 1,
        pathLength: 1,
        transition: {
            duration: 0.5,
            ease: 'easeInOut',
        },
    },
};

const pathVariants2 = {
    hidden: {
        opacity: 0,
        pathLength: 0,
    },
    visible: {
        opacity: 1,
        pathLength: 1,
        transition: {
            duration: 2.5,
            ease: 'easeInOut',
        },
    },
};

const StyledSVGDesktop = styled(motion.svg)`
    display: none;

    @media (min-width: 768px) {
        display: block;
        overflow: visible;
        width: 14rem;
    }
`;

const StyledSVGMobile = styled(motion.svg)`
    overflow: visible;
    width: 4.05rem;

    @media (min-width: 768px) {
        display: none;
    }
`;

export default function DDXAnimatedLogo(): JSX.Element {
    return (
        <>
            <StyledSVGDesktop initial="hidden" viewBox="0 -20 1747.39 172.8" animate="visible">
                <motion.path
                    d="M442.568 1.89502H487.101C528.792 1.89502 558.165 31.2679 558.165 68.221C558.165 105.174 528.792 134.547 487.101 134.547H442.568V1.89502ZM487.101 113.702C516.474 113.702 535.424 94.7514 535.424 68.221C535.424 41.6906 516.474 22.7403 487.101 22.7403H464.361V113.702H487.101Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M620.701 36.0054C648.178 36.0054 669.971 57.7982 669.971 86.2236V92.8562H591.328C594.17 108.964 606.488 118.439 620.701 118.439C640.598 118.439 648.179 105.174 648.179 105.174H669.024C669.024 105.174 658.601 136.442 620.701 136.442C593.223 136.442 571.43 114.649 571.43 86.2236C571.43 57.7982 593.223 36.0054 620.701 36.0054ZM649.047 76.7485C647.152 65.3783 637.677 54.0081 620.622 54.0081C607.356 54.0081 595.039 62.5358 592.196 76.7485H649.047Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M686.948 37.9006H706.845V52.1133H707.793C707.793 52.1133 715.373 36.9531 734.323 36.9531H745.694V55.9034H733.376C718.216 55.9034 706.845 66.3261 706.845 82.4338V134.547H686.948V37.9006Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M770.723 0C777.356 0 782.646 5.29029 782.646 11.9229C782.646 18.5555 777.356 23.6878 770.723 23.6878C764.091 23.6878 758.958 18.5555 758.958 11.9229C758.958 5.29029 764.091 0 770.723 0ZM760.853 37.9006H780.751V134.547H760.853V37.9006Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M814.862 37.9004L835.707 116.544H848.025L868.87 37.9004H889.716L863.185 134.547H820.547L794.017 37.9004H814.862Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M939.934 36.0054C959.831 36.0054 970.254 50.2181 970.254 50.2181H971.202V37.9004H991.099V134.547H971.202V121.282H970.254C970.254 121.282 959.831 136.442 939.934 136.442C914.351 136.442 894.453 116.544 894.453 86.2236C894.453 55.9032 914.351 36.0054 939.934 36.0054ZM943.724 118.439C959.831 118.439 971.202 107.069 971.202 86.2236C971.202 65.3783 959.831 54.0081 943.724 54.0081C926.668 54.0081 915.298 65.3783 915.298 86.2236C915.298 107.069 926.668 118.439 943.724 118.439Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M1015.73 1.89502H1060.27C1101.96 1.89502 1131.33 31.2679 1131.33 68.221C1131.33 105.174 1101.96 134.547 1060.27 134.547H1015.73V1.89502ZM1060.27 113.702C1089.64 113.702 1108.59 94.7514 1108.59 68.221C1108.59 41.6906 1089.64 22.7403 1060.27 22.7403H1037.53V113.702H1060.27Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M1152.18 1.89502H1247.88V22.7403H1173.97V56.8508H1233.66V77.6961H1173.97V113.702H1247.88V134.547H1152.18V1.89502Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M1292.41 69.1685L1259.25 1.89502H1282.93L1310.41 58.7458H1318.94L1346.42 1.89502H1370.11L1336.94 69.1685L1372 134.547H1348.31L1318.94 78.6436H1310.41L1281.04 134.547H1257.35L1292.41 69.1685Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M0 1.89502H56.8508C90.0138 1.89502 116.86 28.7412 116.86 61.9042H0V1.89502Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M129.494 1.89502H186.344C219.507 1.89502 246.354 28.7412 246.354 61.9042H129.494V1.89502Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M0 134.547H56.8508C90.0138 134.547 116.86 107.701 116.86 74.5376H0V134.547Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M129.494 134.547H186.344C219.507 134.547 246.354 107.701 246.354 74.5376H129.494V134.547Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M241.695 1.89502H283.702L376.558 134.547H334.63L241.695 1.89502Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M299.256 1.89502L337.946 57.0877L376.558 1.89502H299.256Z"
                    fill="#EF88AA"
                    variants={pathVariants2}
                />
                <motion.path
                    d="M319.154 134.547L280.464 79.2754L241.853 134.547H319.154Z"
                    fill="#EF88AA"
                    variants={pathVariants2}
                />
            </StyledSVGDesktop>
            {/* </Link> */}

            {/* <Link isExternal href="https://derivadex.com/" aria-label="DerivaDEX"> */}
            <StyledSVGMobile
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 80 1000 168"
                initial="hidden"
                animate="visible"
            >
                <motion.path
                    d="M0 0H147.3C233.2 0 302.8 69.6 302.8 155.5H0V0Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M335.4 0H482.7C568.6 0 638.2 69.6 638.2 155.5H335.4V0Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M0 343.6H147.3C233.2 343.6 302.8 274 302.8 188.1H0V343.6Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path
                    d="M335.4 343.6H482.7C568.6 343.6 638.2 274 638.2 188.1H335.4V343.6Z"
                    fill="#2D1680"
                    variants={pathVariants1}
                />
                <motion.path d="M626.1 0H735L975.4 343.6H866.7L626.1 0Z" fill="#2D1680" variants={pathVariants1} />
                <motion.path d="M775.1 0L875.3 143L975.4 0H775.1Z" fill="#EF88AA" variants={pathVariants2} />
                <motion.path d="M826.7 343.6L726.6 200.5L626.4 343.6H826.7Z" fill="#EF88AA" variants={pathVariants2} />
            </StyledSVGMobile>
            {/* </Link> */}
        </>
    );
}
