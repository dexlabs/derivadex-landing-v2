import {
    Box,
    Button,
    Link,
    List,
    ListItem,
    Popover,
    PopoverContent,
    PopoverTrigger,
    useBreakpointValue,
} from '@chakra-ui/react';
import React from 'react';

export default function ResourcesDropdownHover() {
    // Detect whether the interaction mode should be click (for mobile) or hover (for desktop)
    const triggerMode = useBreakpointValue<'click' | 'hover' | undefined>({ base: 'click', md: 'hover' });

    return (
        <Box color="#211646">
            <Popover trigger={triggerMode} placement="bottom-start">
                <PopoverTrigger>
                    <Button
                        variant="ghost"
                        color="#211646"
                        fontWeight="600"
                        fontSize="0.875rem"
                        textTransform="uppercase"
                        _hover={{ color: 'teal.700' }}
                    >
                        Resources
                    </Button>
                </PopoverTrigger>
                <PopoverContent
                    border={0}
                    boxShadow="md"
                    p={4}
                    rounded="lg"
                    _focus={{ boxShadow: 'md' }}
                    width="100%"
                    bg="white"
                >
                    <List spacing={2}>
                        <ListItem>
                            <Link href="https://testnet-explorer.derivadex.io/" target={'_blank'} isExternal>
                                Explorer
                            </Link>
                        </ListItem>
                        <ListItem>
                            <Link href="https://learn.derivadex.com/" target={'_blank'} isExternal>
                                Learn
                            </Link>
                        </ListItem>
                        <ListItem>
                            <Link href="https://docs.derivadex.io/" target={'_blank'} isExternal>
                                Docs
                            </Link>
                        </ListItem>
                        <ListItem>
                            <Link href="https://testnet.derivadex.io/api-docs" target={'_blank'} isExternal>
                                API Reference
                            </Link>
                        </ListItem>
                        <ListItem>
                            <Link href="https://www.governance.diamonds/" target={'_blank'} isExternal>
                                Governance
                            </Link>
                        </ListItem>
                    </List>
                </PopoverContent>
            </Popover>
        </Box>
    );
}
