'use client';
import { Box, Flex, Text } from '@chakra-ui/react';
import styled from '@emotion/styled';
import { useScrollPosition } from '@n8tb1t/use-scroll-position';
import { ReactNode, useState } from 'react';
import { Link } from 'react-scroll';

import DDXAnimatedLogo from './DDXAnimatedLogo';
import ResourcesDropdown from './ResourcesDropdown';

const Nav = styled(Flex)<{ active: number }>`
    ${({ active }) =>
        active &&
        `
    -webkit-appearance: none;
    -webkit-box-shadow: rgba(0, 0, 0, 0.15) 0 2px 8px;
    box-shadow: rgba(0, 0, 0, 0.15) 0 2px 8px;
  `}
`;

export default function Navbar(): JSX.Element {
    const [atTopOfPage, setAtTopOfPage] = useState(true);

    function LinkComponent({ children }: { children: ReactNode }) {
        return (
            <Text
                backgroundColor="transparent"
                color="#211646"
                textTransform="uppercase"
                display="block"
                fontWeight="600"
                fontSize="0.875rem"
                px="0.5rem"
                whiteSpace="nowrap"
                _hover={{
                    textDecoration: 'underline',
                    cursor: 'pointer',
                }}
            >
                {children}
            </Text>
        );
    }

    useScrollPosition(({ currPos }) => {
        if (currPos.y === 0) setAtTopOfPage(true);
        else setAtTopOfPage(false);
    });

    return (
        <Nav
            as="header"
            w="100%"
            m="0 auto"
            justify="space-between"
            bg="white"
            align="center"
            userSelect="none"
            top="0"
            position="fixed"
            px="1rem"
            h={['3.5rem', '3.5rem', '3rem']}
            minWidth="300px"
            zIndex={2}
            active={!atTopOfPage ? 1 : 0}
        >
            <Flex align="center" justify="space-between" w="100%" h="100%">
                <Flex align="center" justify="center" m="0" p="0">
                    <DDXAnimatedLogo />
                </Flex>
                <Box display={['none', 'none', 'block']}>
                    <Flex as="nav" align="center" flex="nowrap">
                        <Link to={'products'} smooth offset={-50} duration={50}>
                            <LinkComponent>{'PRODUCTS'}</LinkComponent>
                        </Link>
                        <Link to={'whatIsDerivadex?'} smooth offset={-40} duration={50}>
                            <LinkComponent>{'WHAT IS DERIVADEX?'}</LinkComponent>
                        </Link>
                        <ResourcesDropdown />
                        <Link to={'contact'} smooth duration={50}>
                            <LinkComponent>{'CONTACT'}</LinkComponent>
                        </Link>
                    </Flex>
                </Box>
            </Flex>
        </Nav>
    );
}
