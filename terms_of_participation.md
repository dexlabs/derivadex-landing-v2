# TERMS OF PARTICIPATION

**(Effective Date: November 6, 2024)**

Thank you for registering to participate in DerivaDEX Ltd.’s Testnet Trading Competition (the “Competition” or “Event”). The Competition will commence on November 6, 2024, and conclude on December 6, 2024. By registering for and participating in the Competition, you (“Entrant”, “Participant”, “you”, or “your”) hereby agree to the terms and conditions set forth in this Participation Agreement (“Participation Agreement”) between You and DerivaDEX Ltd. (“DerivaDEX”, “Company”, “the Event Administrator”, “We” or “Us”). You are also bound by DerivaDEX’s decisions, which are final and binding in all matters related to the Competition. These Terms constitute a legally binding agreement between you and DerivaDEX Ltd.

Your participation is contingent upon fulfilling all requirements set forth herein, including required due diligence screening, as requested. If you are ineligible or otherwise fail to comply with this Participation Agreement (“Participation Agreement”) you may be disqualified at the sole discretion of the Event Administrator. You are responsible for your own compliance with eligibility criteria.

## 1. Eligibility.

### a. Age Restrictions:

The Competition is open only to individuals who are at least eighteen (18) years old at the time of entry and capable of forming a binding legal relationship with the Event Administrator.

### b. Geographical Restrictions:

The Event Administrator makes no representation that the Competition is appropriate for locations outside of the British Virgin Islands (BVI), and participation in the Competition from territories where Competition-related activities are illegal is prohibited. Those who choose to participate in the Competition from locations outside of BVI do so on their own initiative and are responsible for compliance with all applicable local laws. You may not participate in the Competition in violation of BVI (or other applicable local) laws and regulations.

### c. Specific Restrictions:

You will **NOT** be eligible to participate in the Competition if you are located in, established in, or you are a resident of Iran, North Korea, Syria, Cuba, Crimea, Donetsk, Luhansk. You agree not to participate in this Competition using any technology for the purposes of circumventing these Terms or the technological measures taken to implement them.

The Event Administrator maintains the right to terminate your participation in the Competition if you fail to notify the Event Administrator that you are aware of a relevant regulatory or law enforcement action or investigation against you or are subject to a civil complaint, action or lawsuit.

**Note:** The Event Administrator may, in its sole discretion, restrict or prohibit access from other jurisdictions, including the United States, on Mainnet. Participation in this Competition does not automatically grant access to or use of DerivaDEX’s Mainnet.

### d. Reward Restrictions:

Registration and participation in this Competition do not automatically make you eligible for rewards. Rewards may or may not be provided to eligible participants. In addition to the eligibility requirements set forth above, please be aware that DerivaDEX Ltd. is a BVI entity and governed by BVI laws and regulations.

**Important Notice Regarding Arbitration**: By agreeing to this Participation Agreement, you agree (with limited exceptions) to resolve any dispute between you and DerivaDEX Ltd. through binding, individual arbitration rather than in court. Please review the “Dispute Resolution” section below for more details regarding arbitration.

## 2. Rules and Procedures.

### a. Individual:

-   Eligibility is based on the criteria mentioned in this Participation Agreement. Ineligibility or non-compliance may result in disqualification.
-   Due diligence is required prior to participation.
-   Approved participants will receive testnet USDC for use in the Testnet.
-   Trading ETHP and BTCP on the Sepolia Testnet is permitted.
-   Participants can submit voluntary bug reports or feedback.

### b. Guidelines and Restrictions:

Participants **MUST**:

-   Adhere to the terms of the Participation Agreement.
-   Ensure content does not contain harmful code, inappropriate content, or violate third-party rights.

### c. Intellectual Property:

Participants are not granted any rights over DerivaDEX intellectual property. Any feedback provided becomes the property of DerivaDEX Ltd.

## 3. General Prohibitions and Company’s Enforcement Rights.

DerivaDEX Ltd. reserves the right to suspend or terminate participation and investigate violations.

## 4. Cancellation, Suspension, or Termination of the Competition.

DerivaDEX Ltd. reserves the right to modify or terminate any part of the Competition without notice.

## 5. Representations, Warranties, and Disclaimers.

Participants must secure wallet credentials and acknowledge risks associated with blockchain usage and participation in the Competition.

## 6. Taxes.

Participants are responsible for any applicable taxes from their participation.

## 7. Indemnity.

Participants agree to indemnify DerivaDEX Ltd. and related entities against any claims related to participation.

## 8. Limitation of Liability.

DerivaDEX Ltd. limits liability for damages or losses incurred during participation.

## 9. Dispute Resolution.

Disputes will be settled by arbitration under BVI IAC Arbitration Rules in Tortola, British Virgin Islands.

## 10. General Conditions.

DerivaDEX Ltd. reserves the right to modify the Competition and enforce terms at its discretion.

## 11. Force Majeure.

DerivaDEX Ltd. is not liable for breaches caused by unforeseen events beyond its control.

## 12. Contact Information.

For any questions or concerns, contact: info@derivadex.com.
